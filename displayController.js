
function displaycontroller($scope, dataStoreService ) {

    $scope.clientData = [];

    $scope.highlightFilteredHeader = function( row, rowRenderIndex, col ) {
        if( col.filters[0].term ){
            return 'header-filtered';
        } else {
            return '';
        }
    };

    $scope.gridOptions = {
        enableFiltering : true,
        enableGridMenu : true,
        onRegisterApi: function(gridApi){

            $scope.gridApi = gridApi;
        },

        columnDefs: [
            {field: 'name', headerCellClass: $scope.highlightFilteredHeader },

            {field: 'email', headerCellClass: $scope.highlightFilteredHeader },

            {field: 'mobile', headerCellClass: $scope.highlightFilteredHeader }
            ]


    };

    $scope.clientData = dataStoreService.getData("dataKey");
    console.log('A',$scope.clientData);

    $scope.gridOptions.data = $scope.clientData;
    console.log('gridOptions data',$scope.gridOptions.data);

    // $scope.clientData = dataStoreService.getItem("dataKey");

}



var app=angular.module('app1');

var require = [
    '$scope',
    'dataStoreService',
    displaycontroller

];



app.controller('displaycontroller', require);
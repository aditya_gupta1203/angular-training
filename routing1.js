var app=angular.module('app1');

app.config(function($routeProvider) {
    $routeProvider
        .when("/", {
            templateUrl : "inputData.html"
        })
        .when("/display", {

            templateUrl : "displayData.html"

        });
});